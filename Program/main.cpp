#include <QApplication>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "blynk.h"
#include <QThread>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

using namespace cv;

#define ENABLE_TEST_HSV 0
#define CHECK_ON_OFF 1
#define ENABLE_MASK 0

//RED
int HSV_low[]  = {0,0,255};
int HSV_high[] = {30,255,255};
int lineCount = 0;

Mat colorDetect(const Mat& t_source){
    Mat src = t_source;
    Mat hsv_src;
    Mat hsv_thresh;
    blur(src,src,Size(5,5));
    GaussianBlur(src,src,Size(3,3),0,0);
    cvtColor(src,hsv_src,COLOR_BGR2HSV);
    //Create trackbar
#if (ENABLE_TEST_HSV)
    std::string hsv_colorDetect = "Color_Detect";
    namedWindow(hsv_colorDetect);
    moveWindow(hsv_colorDetect, 200, 40);

    createTrackbar("lowH",hsv_colorDetect, &HSV_low[0], 255);
    createTrackbar("highH",hsv_colorDetect, &HSV_high[0], 255);
    createTrackbar("lowS",hsv_colorDetect, &HSV_low[1], 255);
    createTrackbar("highS",hsv_colorDetect, &HSV_high[1], 255);
    createTrackbar("lowV",hsv_colorDetect, &HSV_low[2], 255);
    createTrackbar("highV",hsv_colorDetect, &HSV_high[2], 255);
#endif

    inRange(hsv_src,Scalar(HSV_low[0],HSV_low[1],HSV_low[2]),Scalar(HSV_high[0],HSV_high[1],HSV_high[2]),hsv_thresh);
    medianBlur(hsv_thresh,hsv_thresh,3);
    Mat kernal_open = getStructuringElement( MORPH_RECT , Size(3,3), Point(-1,-1) );
    morphologyEx(hsv_thresh,hsv_thresh,MORPH_OPEN,kernal_open);
    Mat kernal_close = getStructuringElement( MORPH_RECT , Size(3,3), Point(-1,-1) );
    morphologyEx(hsv_thresh,hsv_thresh,MORPH_CLOSE,kernal_close);

    return hsv_thresh;
}

Mat threshMask(const Mat& t_source){
    Mat src = t_source;
    Mat graySource;
    Mat mask_bright_OTSU;
    Mat mask_color;
    Mat mask_total;

    GaussianBlur(src,src,Size(3,3),0,0);
    cvtColor(src,graySource,COLOR_BGR2GRAY);

    threshold(graySource,mask_bright_OTSU,255,255,THRESH_BINARY | THRESH_OTSU);
    dilate(mask_bright_OTSU, mask_total, Mat(), Point(-1, -1), 3, 1, 1);
    mask_color = colorDetect(src);
    bitwise_and(mask_bright_OTSU,mask_color,mask_total);

    medianBlur(mask_total,mask_total,3);
    //Total mask
    Mat kernal_open = getStructuringElement( MORPH_RECT , Size(3,3), Point(-1,-1) );
    morphologyEx(mask_total,mask_total,MORPH_OPEN,kernal_open);
    Mat kernal_close = getStructuringElement( MORPH_RECT , Size(3,3), Point(-1,-1) );
    morphologyEx(mask_total,mask_total,MORPH_CLOSE,kernal_close);
#if (ENABLE_MASK)
    imshow("Mask",mask_total);
    moveWindow("Mask", 50, 50);
#endif
    return mask_total;
}


bool isOn(const Mat& t_source){

    Mat src = t_source;
    src = threshMask(src);

    double white_pixel;
    white_pixel = countNonZero(src);
    if(white_pixel > 2000){
        LOG << "ON";
        return true;
    }
    LOG << "OFF";
    return false;
}

void writeToFile(QFile* file,bool isSystemOn){
    QString data;
    QTextStream stream(file);
    QString systemStatus = isSystemOn ? "ON" : "OFF";

    if (file->open(QIODevice::ReadWrite | QIODevice::Append)) {
        if(::lineCount >= 20){
            file->resize(0);
            ::lineCount = 0;
        }
        stream << QDateTime::currentDateTime().toString() <<" "<<systemStatus<< endl;
        ::lineCount++;
        file->close();
    }
    else{
       LOG << "Can not open file";
    }
}

void readLineCount(QFile* file){
    QTextStream stream(file);
    if(file->open(QIODevice::ReadOnly)){
        while(!stream.atEnd()){
            stream.readLine();
            ::lineCount++;
        }
        file->close();
    }
    else{
        LOG << "Can not read file";
    }
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    /**********************************************/
    Blynk blynk;
    VideoCapture cap;
    QFile file("data.txt");
    Mat frame;
    bool isSetWarning = false;
    bool system_ON = false;

    blynk.Request_SetPinValue("V4","0");
    QThread::msleep(100);
    blynk.Request_SetPinValue("D8","0");
    readLineCount(&file);

#if (ENABLE_TEST_HSV)
    if(!cap.open(1)){  // Open Camera index 0 - 1
        return 0;
    }
    while (true)
    {
        Mat result;
        cap >> frame;
        if( frame.empty() ) {
            break;          // end of video stream
        }
        //result = colorDetect(frame);
        result = threshMask(frame);
        imshow("Color_Detect",result);
        imshow("Frame",frame);

        if( waitKey(10) == 32 ) {
            destroyAllWindows();
            break;           // stop capturing by pressing : asci ESC= 27, Space  = 32, Enter = 13
        }
    }
#endif

#if (CHECK_ON_OFF)
    if(!cap.open(1)){  // Open Camera index 0 - 1
        LOG << "Can not open Camera";
        return 0;
    }
    while (true)
    {
        cap >> frame;
        if( frame.empty() ) {
            break;          // end of video stream
        }
        system_ON = isOn(frame);

        if(system_ON){
            if(isSetWarning){
                writeToFile(&file,system_ON);
                blynk.Request_SetPinValue("D8","0");
                QThread::msleep(100);
                blynk.Request_SetPinValue("V4","0");
                isSetWarning = false;
            }
        }
        else{
            if(!isSetWarning){
                writeToFile(&file,system_ON);
                blynk.Request_SetPinValue("D8","1");
                QThread::msleep(100);
                blynk.Request_SetPinValue("V4","255");
                isSetWarning = true;
            }
        }
        imshow("Frame",frame);
        if( waitKey(10) == 32 ) {
            destroyAllWindows();
            break;           // stop capturing by pressing : asci ESC= 27, Space  = 32, Enter = 13
        }
    }
#endif
    return 0;

    /***********************************************/
    return app.exec();
}
