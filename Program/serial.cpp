#include "serial.h"

Serial::Serial(QObject *parent) : QObject(parent)
{
    m_serial = new QSerialPort();
    m_buffer = new QByteArray();
}

Serial::~Serial()
{
    if(m_serial->isOpen()){
            m_serial->close();
    }

    delete  m_serial;
    delete m_buffer;


}

void Serial::init(QString portName = "COM1")
{
#if (DEF_LOG)
    LOG << Q_FUNC_INFO;
#endif
    m_serial->setPortName(portName);
    m_serial->open(QIODevice::ReadWrite);
    m_serial->setBaudRate(QSerialPort::Baud9600);
    m_serial->setDataBits(QSerialPort::Data8);
    m_serial->setParity(QSerialPort::NoParity);
    m_serial->setStopBits(QSerialPort::OneStop);
    m_serial->setFlowControl(QSerialPort::HardwareControl);

    if(m_serial->isOpen()){
        LOG << "Serial is connected";
    }
    else{
        LOG << "Serial is busy";
    }
}

void Serial::WriteData(QByteArray data)
{
#if (DEF_LOG)
    LOG << Q_FUNC_INFO;
#endif
    qint64 result = m_serial->write(data);
    if(result == -1){
        LOG << "Write : Failed";
    }
    else{
        LOG << "Write : " <<result << "Byte";
    }
}

QByteArray Serial::ReadData()
{
#if (DEF_LOG)
    LOG << Q_FUNC_INFO;
#endif
    QByteArray result = m_serial->readAll();
    if(result.isEmpty()){
        LOG << "Read : Failed";
        return QByteArray("");
    }
    else{
        LOG << "Read : " << result;
    }
    return result;
}

void Serial::availablePort()
{
#if (DEF_LOG)
    LOG << Q_FUNC_INFO;
#endif
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    if(ports.isEmpty()){
        LOG << "Ports Empty";
        return;
    }
    for(QSerialPortInfo port : ports){
        LOG << "PortName : " << port.portName();
        LOG << "Description : " << port.description();
    }
}

