#include "net.h"

NetAPI::NetAPI(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}

void NetAPI::replyFinished(QNetworkReply *reply)
{
    respond = reply->readAll();
    emit sigRequestCompleted(respond);
}

void NetAPI::request(QString url)
{
  QUrl qrl(url);
  manager->get(QNetworkRequest(qrl));
}
