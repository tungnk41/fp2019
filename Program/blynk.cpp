#include "blynk.h"

Blynk::Blynk(QObject *parent) : QObject(parent)
{
    netAPI = new NetAPI(this);
    m_authToken = "VtsN32RpDD2uN5D0qXWivFhnaP02OyUf";
    connect(netAPI,SIGNAL(sigRequestCompleted(QString)),this,SLOT(respond(QString)));
}

void Blynk::setAuthToken(QString auth)
{
    m_authToken = auth;
}

void Blynk::Request_getPinValue(QString pin)
{

    QString url = QString("http://blynk-cloud.com/%1/get/%2").arg(m_authToken).arg(pin);
    netAPI->request(url);
}

void Blynk::Request_SetPinValue(QString pin, QString value)
{

    QString url = QString("http://blynk-cloud.com/%1/update/%2?value=%3").arg(m_authToken).arg(pin).arg(value);
    netAPI->request(url);
}

void Blynk::Request_SetWidgetProperty(QString pin, QString property, QString value)
{

    QString url = QString("http://blynk-cloud.com/%1/update/%2?%3=%4").arg(m_authToken).arg(pin).arg(property).arg(value);
    netAPI->request(url);
}

void Blynk::Request_isHardwareConnected()
{

    QString url = QString("http://blynk-cloud.com/%1/isHardwareConnected").arg(m_authToken);
    netAPI->request(url);
}

void Blynk::Request_isAppConnected()
{
    QString url = QString("http://blynk-cloud.com/%1/isAppConnected").arg(m_authToken);
    netAPI->request(url);
}

void Blynk::Request_ProjectData()
{

    QString url = QString("http://blynk-cloud.com/%1/project").arg(m_authToken);
    netAPI->request(url);
}

QString Blynk::getPinValue(QString pin)
{
    return m_respond;
}

void Blynk::isHardwareConnected()
{

}

void Blynk::isAppConnected()
{

}

void Blynk::ProjectData()
{

}

void Blynk::respond(QString respond)
{
    m_respond = respond;
}
