#ifndef BLYNK_H
#define BLYNK_H

#include <QObject>
#include "net.h"
#include "log.h"

class Blynk : public QObject
{
    Q_OBJECT
public:
    explicit Blynk(QObject *parent = 0);
    void setAuthToken(QString auth);
    void Request_getPinValue(QString pin);
    void Request_SetPinValue(QString pin,QString value = 0);
    void Request_SetWidgetProperty(QString pin, QString property, QString value);
    void Request_isHardwareConnected();
    void Request_isAppConnected();
    void Request_ProjectData();
signals:

public slots:
    QString getPinValue(QString pin);
    void isHardwareConnected();
    void isAppConnected();
    void ProjectData();
    void respond(QString);
private:
    NetAPI* netAPI;
    QString m_authToken;
    QString m_respond;
};

#endif // BLYNK_H
