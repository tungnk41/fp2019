QT += core serialport widgets network
QT -= gui

CONFIG += c++11

TARGET = Program
#CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    serial.h \
    net.h \
    blynk.h \
    log.h

SOURCES += main.cpp \
    serial.cpp \
    net.cpp \
    blynk.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += -L$$PWD/lib/
LIBS += -L$$PWD/lib/

Release: {
        LIBS += -llibopencv_calib3d411.dll \
                -llibopencv_core411.dll \
                -llibopencv_dnn411.dll \
                -llibopencv_features2d411.dll \
                -llibopencv_flann411.dll \
                -llibopencv_gapi411.dll \
                -llibopencv_highgui411.dll \
                -llibopencv_imgcodecs411.dll \
                -llibopencv_imgproc411.dll \
                -llibopencv_ml411.dll \
                -llibopencv_objdetect411.dll \
                -llibopencv_photo411.dll \
                -llibopencv_stitching411.dll \
                -llibopencv_video411.dll \
                -llibopencv_videoio411.dll
}

DISTFILES += \
    data.txt




