#ifndef NET_H
#define NET_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include "log.h"

class NetAPI : public QObject
{
    Q_OBJECT
public:
    explicit NetAPI(QObject *parent = nullptr);
private:
    QNetworkAccessManager *manager;
    QString respond;
public:
    void request(QString url);
signals:
    void sigRequestCompleted(QString);
public slots:
    void replyFinished(QNetworkReply *);
};

#endif // NET_H
