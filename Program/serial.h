#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "log.h"

class Serial : public QObject
{
    Q_OBJECT
public:
    explicit Serial(QObject *parent = nullptr);
    ~Serial();

    void init(QString);

    void WriteData(QByteArray data);
    QByteArray ReadData();
    void static availablePort();

signals:

public slots:

private:
    QSerialPort* m_serial;
    QByteArray* m_buffer;
};

#endif // SERIAL_H
