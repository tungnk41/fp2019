#include <Arduino.h>
#include <TM1637Display.h>
#include <ESP8266_Lib.h>
#include <BlynkSimpleShieldEsp8266.h>
#include <DHT.h>

#define ESP8266_BAUD 115200
#define EspSerial Serial
#define BLYNK_PRINT Serial
#define DHT_PIN 2
#define DHT_TYPE DHT11
#define LED 8

ESP8266 wifi(&EspSerial);

BLYNK_READ(V0);
BLYNK_READ(V1);

char ssid[] = "Blynk";
char pass[] = "87654321";
char auth[] = "VtsN32RpDD2uN5D0qXWivFhnaP02OyUf";

DHT dht(DHT_PIN, DHT_TYPE);
BlynkTimer timer;

#define CLK 4
#define DIO 3
uint8_t dataDisplay;
TM1637Display display(CLK, DIO);

int humidity = 0;
int temperature = 0;

void sendSensor()
{
  humidity = dht.readHumidity();
  temperature = dht.readTemperature(); // or dht.readTemperature(true) for Fahrenheit

  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  else{
     Blynk.virtualWrite(V0,temperature);
     delay(100);
     Blynk.virtualWrite(V1,humidity);
     delay(100);
     display.showNumberDecEx(temperature*100+humidity, 0b01000000, false, 4, 0);
     delay(100);
  }
}

void setup()
{
  Serial.begin(115200);
  delay(10);
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  Blynk.begin(auth, wifi, ssid, pass);

  delay(10);
  dht.begin();
  timer.setInterval(1000L, sendSensor);
  pinMode(LED,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  display.setBrightness(0x0f);
}

void loop()
{
    Blynk.run();
    timer.run();
}



